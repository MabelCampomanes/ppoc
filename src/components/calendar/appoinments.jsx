export const appointments = [
    {
      title: 'Website Re-Design Plan',
      startDate: new Date(2023, 6, 23, 9, 30),
      endDate: new Date(2023, 6, 23, 11, 30),
    }, {
      title: 'Book Flights to San Fran for Sales Trip',
      startDate: new Date(2023, 6, 23, 12, 0),
      endDate: new Date(2023, 6, 23, 13, 0),
    }, {
      title: 'Install New Router in Dev Room',
      startDate: new Date(2023, 6, 23, 14, 30),
      endDate: new Date(2023, 6, 23, 15, 30),
    }, {
      title: 'Approve Personal Computer Upgrade Plan',
      startDate: new Date(2023, 6, 24, 10, 0),
      endDate: new Date(2023, 6, 24, 11, 0),
    }, {
      title: 'Final Budget Review',
      startDate: new Date(2023, 6, 24, 12, 0),
      endDate: new Date(2023, 6, 24, 13, 35),
    }, {
      title: 'New Brochures',
      startDate: new Date(2023, 6, 24, 14, 30),
      endDate: new Date(2023, 6, 24, 15, 45),
    }, {
      title: 'Install New Database',
      startDate: new Date(2023, 6, 25, 9, 45),
      endDate: new Date(2023, 6, 25, 11, 15),
    }, {
      title: 'Approve New Online Marketing Strategy',
      startDate: new Date(2023, 6, 25, 12, 0),
      endDate: new Date(2023, 6, 25, 14, 0),
    }, {
      title: 'Upgrade Personal Computers',
      startDate: new Date(2023, 6, 25, 15, 15),
      endDate: new Date(2023, 6, 25, 16, 30),
    }, {
      title: 'Customer Workshop',
      startDate: new Date(2023, 6, 26, 11, 0),
      endDate: new Date(2023, 6, 26, 12, 0),
    }, {
      title: 'Prepare 2015 Marketing Plan',
      startDate: new Date(2023, 6, 26, 11, 0),
      endDate: new Date(2023, 6, 26, 13, 30),
    }, {
      title: 'Brochure Design Review',
      startDate: new Date(2023, 6, 26, 14, 0),
      endDate: new Date(2023, 6, 26, 15, 30),
    }
  ];
  