import React from 'react';
import moment from 'moment';

export const Calendario = () => {
  // Obtenemos los días del mes actual
  const diasDelMes = [];
  const diasEnElMes = moment().daysInMonth();
  for (let dia = 1; dia <= diasEnElMes; dia++) {
    diasDelMes.push(dia);
  }

  // Creamos las filas para representar las horas
  const horas = [];
  for (let hora = 0; hora < 24; hora++) {
    horas.push(
      <tr key={hora}>
        <td>{hora}:00</td>
        {diasDelMes.map((dia) => (
          <td key={dia}>
            <CeldaCalendario dia={dia} hora={hora} />
          </td>
        ))}
      </tr>
    );
  }

  return (
    <table>
      <thead>
        <tr>
          <th></th>
          {diasDelMes.map((dia) => (
            <th key={dia}>{dia}</th>
          ))}
        </tr>
      </thead>
      <tbody>{horas}</tbody>
    </table>
  );
};

const CeldaCalendario = ({ dia, hora }) => {
  // Aquí podríamos hacer alguna lógica para determinar si hay un evento programado para esa hora y día en particular.
  return <div></div>;
};

const Evento = ({ titulo, horaInicio, horaFin }) => {
  // Renderizamos un componente de evento con su título, hora de inicio y hora de fin.
  return (
    <div>
      <p>{titulo}</p>
      <p>{horaInicio} - {horaFin}</p>
    </div>
  );
};