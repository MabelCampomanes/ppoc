import * as React from 'react';
import { styled } from '@mui/material/styles';
import Paper from '@mui/material/Paper';
import { green, lightBlue } from '@mui/material/colors';
import Checkbox from '@mui/material/Checkbox';
import FormControlLabel from '@mui/material/FormControlLabel';
import TextField from '@mui/material/TextField';
import MenuItem from '@mui/material/MenuItem';
import {
  ViewState, EditingState, GroupingState, IntegratedGrouping, IntegratedEditing,
} from '@devexpress/dx-react-scheduler';
import {
  Scheduler,
  Resources,
  Appointments,
  AppointmentTooltip,
  AppointmentForm,
  DragDropProvider,
  GroupingPanel,
  WeekView,
  MonthView,
  Toolbar,
  ViewSwitcher,
  DateNavigator,
  TodayButton, 
} from '@devexpress/dx-react-scheduler-material-ui';
import { data as appointments } from '../turnos/turnos';

const PREFIX = 'Demo';
// #FOLD_BLOCK
const classes = {
  container: `${PREFIX}-container`,
  formControlLabel: `${PREFIX}-formControlLabel`,
  text: `${PREFIX}-text`,
};

const StyledDiv = styled('div')(({ theme }) => ({
    [`&.${classes.container}`]: {
      display: 'flex',
      marginBottom: theme.spacing(2),
      justifyContent: 'flex-end',
    },
    [`& .${classes.text}`]: {
      ...theme.typography.h6,
      marginRight: theme.spacing(2),
    },
  }));

// #FOLD_BLOCK
const StyledFormControlLabel = styled(FormControlLabel)(({
  theme: { spacing, palette, typography },
}) => ({
  [`&.${classes.formControlLabel}`]: {
    padding: spacing(2),
    paddingLeft: spacing(10),
  },
  [`&.${classes.text}`]: {
    ...typography.caption,
    color: palette.text.secondary,
    fontWeight: 'bold',
    fontSize: '1rem',
  },
}));

const isWeekOrMonthView = viewName => viewName === 'Week' || viewName === 'Month';

const priorityData = [
  { text: 'Barajas', id: 1, color: lightBlue },
  { text: 'Alameda de Osuna', id: 2, color: green },
];

const GroupOrderSwitcher = (({ isGroupByDate, onChange }) => (
  <StyledFormControlLabel
    control={
      <Checkbox checked={isGroupByDate} onChange={onChange} color="primary" />
    }
    label="Mostrar por fecha"
    className={classes.formControlLabel}
    classes={{ label: classes.text }}
  />
));

const LocaleSwitcher = (
    ({ onLocaleChange, currentLocale }) => (
      <StyledDiv className={classes.container}>
        <div className={classes.text}>

        </div>
        <TextField
          select
          variant="standard"
          value={currentLocale}
          onChange={onLocaleChange}
        >
          <MenuItem value="es-ES">Español</MenuItem>          
          <MenuItem value="en-US">English</MenuItem>
        </TextField>
      </StyledDiv>
    )
  );

export default class Scheduling extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      data: appointments.filter(appointment => appointment.priorityId < 3),
      resources: [{
        fieldName: 'priorityId',
        title: 'Priority',
        instances: priorityData,
      }],
      grouping: [{
        resourceName: 'priorityId',
      }],
      groupByDate: isWeekOrMonthView,
      isGroupByDate: true,
      locale: 'es-ES',
    };
    this.changeLocale = event => this.setState({ locale: event.target.value });
    this.commitChanges = this.commitChanges.bind(this);
    this.onGroupOrderChange = () => {
      const { isGroupByDate } = this.state;
      this.setState({
        isGroupByDate: !isGroupByDate,
        groupByDate: isGroupByDate ? undefined : isWeekOrMonthView,
      });
    };
    this.currentDateChange = (currentDate) => { this.setState({ currentDate }); };
  }

  commitChanges({ added, changed, deleted }) {
    this.setState((state) => {
      let { data } = state;
      if (added) {
        const startingAddedId = data.length > 0 ? data[data.length - 1].id + 1 : 0;
        data = [...data, { id: startingAddedId, ...added }];
      }
      if (changed) {
        data = data.map(appointment => (
          changed[appointment.id] ? { ...appointment, ...changed[appointment.id] } : appointment));
      }
      if (deleted !== undefined) {
        data = data.filter(appointment => appointment.id !== deleted);
      }
      return { data };
    });
  }
  

  render() {
    const {
      data, resources, grouping, groupByDate, isGroupByDate, currentDate, locale
    } = this.state;

    return (
      (
        <div>
        <LocaleSwitcher
          currentLocale={locale}
          onLocaleChange={this.changeLocale}
            />
          <GroupOrderSwitcher isGroupByDate={isGroupByDate} onChange={this.onGroupOrderChange} />
          <Paper >
            <Scheduler
              data={data}
              height={660}
              locale={locale}
              firstDayOfWeek={1}
            >
              <ViewState
                currentDate={currentDate}
                onCurrentDateChange={this.currentDateChange}
              />
              <EditingState
                onCommitChanges={this.commitChanges}
              />
              <GroupingState
                grouping={grouping}
                groupByDate={groupByDate}
              />

              <MonthView />

              <WeekView
                startDayHour={10}
                endDayHour={20}
              />
              

              <Appointments />
              <Resources
                data={resources}
                mainResourceName="priorityId"
              />
              <IntegratedGrouping />
              <IntegratedEditing />

              <AppointmentTooltip />
              <AppointmentForm />

              <Toolbar 

              />
              <DateNavigator/>
              <TodayButton />
              <ViewSwitcher />
              <GroupingPanel />
              <DragDropProvider />
            </Scheduler>
          </Paper>
        </div>
      )
    );
  }
}
